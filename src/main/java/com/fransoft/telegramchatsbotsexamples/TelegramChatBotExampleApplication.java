package com.fransoft.telegramchatsbotsexamples;

import com.fransoft.telegramchatbot.utils.EmojiService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableFeignClients
@EnableCaching
@EnableScheduling
@EntityScan(basePackages = {"com.fransoft.telegramchatsbotsexamples.persistence.entity"})
@EnableJpaRepositories("com.fransoft.telegramchatsbotsexamples.persistence.repository")

@SpringBootApplication
public class TelegramChatBotExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelegramChatBotExampleApplication.class, args);
	}

	@Bean
	public EmojiService emojiService(){
		return new EmojiService();
	}
}
