package com.fransoft.telegramchatsbotsexamples.bots.basic;

import com.fransoft.telegramchatbot.AbstractTelegramLongPollingBot;
import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class BitfinexBot extends AbstractTelegramLongPollingBot {

    private static final String COMMAND_PAIRS = "pairs";
    private static final String COMMAND_TICKERS = "ticker";
    private static final String COMMAND_SET_ALARM = "setalarm";
    private static final String COMMAND_DELETE_ALARM = "deletealarm";
    private static final String COMMAND_SHOW_ALARMS = "showalarms";

    BitfinexBotProcessor bitfinexBotProcessor;

    public BitfinexBot(
            BitfinexBotProcessor bitfinexBotProcessor,
            @Value("${bitfinexbot.name}") String botUsername,
            @Value("${bitfinexbot.apikey}") String apiKey,
            @Value("${bitfinexbot.function}") String botFunction,
            @Value("${bitfinexbot.restricted_users}") String restrictedUsers) {
    	super(botUsername, apiKey, botFunction);
        this.setRestrictedUsers(restrictedUsers);
        this.bitfinexBotProcessor = bitfinexBotProcessor;

        initCommands();
    }

    private void initCommands() {
        CommandData commandDataPairs = CommandData.builder()
                .verb("/"+COMMAND_PAIRS)
                .options(null)
                .presentation("get pairs list")
                .build();
        CommandData commandDataTickers = CommandData.builder()
                .verb("/"+COMMAND_TICKERS)
                .options(getTickersOptions())
                .presentation("get pair ticker in this time")
                .build();
        CommandData commandDataSetAlarm = CommandData.builder()
                .verb("/"+COMMAND_SET_ALARM)
                .options(setAlarmOptions())
                .presentation("set an alarm on a pair with a price")
                .build();
        CommandData commandDataDeleteAlarm = CommandData.builder()
                .verb("/"+COMMAND_DELETE_ALARM)
                .options(deleteAlarmOptions())
                .presentation("delete an alarm on a pair with a price")
                .build();

        CommandData commandDataShowAlarms = CommandData.builder()
                .verb("/"+COMMAND_SHOW_ALARMS)
                .options(null)
                .presentation("show all user alarms")
                .build();


        addCommand(COMMAND_PAIRS, commandDataPairs);
        addCommand(COMMAND_TICKERS, commandDataTickers);
        addCommand(COMMAND_SET_ALARM, commandDataSetAlarm);
        addCommand(COMMAND_DELETE_ALARM, commandDataDeleteAlarm);
        addCommand(COMMAND_SHOW_ALARMS, commandDataShowAlarms);
    }

    private Options deleteAlarmOptions() {
        Options options = new Options();
        Option option = new Option("i", "id", true, "alarm id");
        option.setRequired(true);
        options.addOption(option);

        return options;
    }

    private Options setAlarmOptions() {
        Options options = new Options();
        Option option = new Option("b", "base", true, "base symbol");
        option.setRequired(true);
        options.addOption(option);
        Option option2 = new Option("r", "reference", true, "reference symbol");
        option2.setRequired(true);
        options.addOption(option2);
        Option option3 = new Option("v", "value", true, "alarm value");
        option3.setRequired(true);
        options.addOption(option3);
        Option option4 = new Option("m", "max", false, "max value");
        option4.setRequired(false);
        options.addOption(option4);
        Option option5 = new Option("i", "min", false, "min value");
        option5.setRequired(false);
        options.addOption(option5);

        return options;    
    }

    private Options getTickersOptions() {
        Options options = new Options();
        Option option = new Option("b", "base", true, "base symbol");
        option.setRequired(true);
        options.addOption(option);
        Option option2 = new Option("r", "reference", true, "reference symbol");
        option2.setRequired(true);
        options.addOption(option2);
        return options;

    }

    @Override
    protected void onReceived(final Update update, final TelegramCommand tc) {
        switch(tc.getVerb()){
            case COMMAND_TICKERS:
                processTickers(update);
                break;
            case COMMAND_PAIRS:
                processPairs(update);
                break;
            case COMMAND_SET_ALARM:
                processSetAlarm(update);
                break;
            case COMMAND_DELETE_ALARM:
                processDeleteAlarm(update);
                break;
            case COMMAND_SHOW_ALARMS:
                processShowAlarms(update);
                break;
            case COMMAND_HELP:
                printHelp(tc);
                break;
            default:
                noImplement(tc);
        }
    }

    public void processShowAlarms(Update update) {
        this.sendMessage(this.bitfinexBotProcessor.showAlarms(TelegramCommand.build(update)));
    }

    public void processDeleteAlarm(Update update) {
        CommandData commandData = getCommand(COMMAND_DELETE_ALARM);
        try {
            this.sendMessage(this.bitfinexBotProcessor.deleteAlarm(TelegramCommand.build(update, commandData.getOptions())));
        } catch (ParseException e) {
            printMessage(TelegramCommand.build(update), CommandLineUtils
                .getHelpCommandLine(commandData.getVerb(), commandData.getOptions(), commandData.getPresentation()));
        }
    }

    public void processSetAlarm(Update update) {
        CommandData commandData = getCommand(COMMAND_SET_ALARM);
        try {
            this.sendMessage(this.bitfinexBotProcessor.setAlarm(TelegramCommand.build(update, commandData.getOptions())));
        } catch (ParseException e) {
            printMessage(TelegramCommand.build(update), CommandLineUtils.getHelpCommandLine(commandData.getVerb(), commandData.getOptions(), commandData.getPresentation()));
        }
    }

    public void processPairs(Update update) {
        this.sendMessage(this.bitfinexBotProcessor.getPairExchanges(TelegramCommand.build(update)));
    }

    public void processTickers(Update update) {
        CommandData commandData = getCommand(COMMAND_TICKERS);
        Options options = commandData.getOptions();
        try {
            this.sendMessage(this.bitfinexBotProcessor.getTicker(TelegramCommand.build(update, options)));
        } catch (ParseException e) {
            printMessage(TelegramCommand.build(update), CommandLineUtils.getHelpCommandLine(commandData.getVerb(), options, commandData.getPresentation()));
        }
    }
}
