package com.fransoft.telegramchatsbotsexamples.bots.basic;

import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.utils.EmojiCtes;
import com.fransoft.telegramchatbot.utils.EmojiService;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.BitfinexFC;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.converter.BitfinexToPojo;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex.PairExchange;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex.Ticker;
import com.fransoft.telegramchatsbotsexamples.persistence.entity.DbAlarm;
import com.fransoft.telegramchatsbotsexamples.persistence.repository.AlarmRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Component
public class BitfinexBotProcessor {
    private static final Logger logger = LoggerFactory.getLogger(BitfinexBotProcessor.class);

    BitfinexFC bitfinexFC;
    BitfinexToPojo bitfinexToPojo;
    AlarmRepo alarmRepo;
    EmojiService emojiService;

    public BitfinexBotProcessor(BitfinexFC bitfinexFC, BitfinexToPojo bitfinexToPojo,
        AlarmRepo alarmRepo, EmojiService emojiService) {
        this.bitfinexFC = bitfinexFC;
        this.bitfinexToPojo = bitfinexToPojo;
        this.alarmRepo = alarmRepo;
        this.emojiService = emojiService;
    }

    public SendMessage setAlarm(TelegramCommand telegramCommand){
        PairExchange pairExchange = new PairExchange(telegramCommand.getParams().getOptionValue("b"),
                telegramCommand.getParams().getOptionValue("r"));

        DbAlarm dbAlarm = DbAlarm.builder()
                .base(pairExchange.getBase())
                .reference(pairExchange.getRef())
                .chatId(telegramCommand.getChatId())
                .userId(telegramCommand.getUserId())
                .maxvalue(telegramCommand.getParams().hasOption("m")? new BigDecimal(telegramCommand.getParams().getOptionValue("v")):null)
                .minvalue(telegramCommand.getParams().hasOption("i")? new BigDecimal(telegramCommand.getParams().getOptionValue("v")):null)
                .alarmDate(new Timestamp(System.currentTimeMillis()))
                .build();
        DbAlarm dbAlarmSaved = alarmRepo.save(dbAlarm);

        return telegramCommand.sendMessage(String.format(
                "Alarm saved with id: %d %s",dbAlarmSaved.getId(), emojiService.getEmojiUnicode(
                EmojiCtes.MORNING_ALARM_CLOCK)), false);
    }

    public SendMessage deleteAlarm(TelegramCommand telegramCommand){
        try{
            alarmRepo.deleteById(Long.valueOf(telegramCommand.getParams().getOptionValue("i")));
        }catch(EmptyResultDataAccessException e){
            return telegramCommand.sendMessage("id no exists", false);
        }
        return telegramCommand.sendMessage(
                String.format("Alarm deleted %s", emojiService.getEmojiUnicode(EmojiCtes.MORNING_ALARM_CLOCK)), false);
    }

    public SendMessage getTicker(TelegramCommand telegramCommand){
        SendMessage sendMessage;
        if (this.status() != BitfinexStatus.READY){
            sendMessage = telegramCommand.sendMessage(
                    String.format("Bitfinex api services down %s", emojiService.getEmojiUnicode(EmojiCtes.BROKEN_HEART)), false);
        }else{
            Ticker ticker = getTicker(new PairExchange(telegramCommand.getParams().getOptionValue("b"), telegramCommand.getParams().getOptionValue("r")));
            if (ticker == null){
                sendMessage = telegramCommand.sendMessage(
                        String.format("no exists pair %s", emojiService.getEmojiUnicode(EmojiCtes.SAD_CRY)), false);
            }else{
                sendMessage = telegramCommand.sendMessage(
                        String.format("%s <b>%s:%s</b> -> %f %s",
                                emojiService.getEmojiUnicode(EmojiCtes.MONEY_DOLLAR),
                                ticker.getBase(), ticker.getReference(), ticker.getLast(),
                                emojiService.getEmojiUnicode(EmojiCtes.KISSING)
                        ), true);
            }
        }

        return sendMessage;
    }

    public Ticker getTicker(PairExchange pair) {
        var result = bitfinexFC.ticker(String.format("t%s%s", pair.getBase(), pair.getRef()));
        if (result.equals("[]")){
            return null;
        }
        return bitfinexToPojo.convertToListTickers(result).get(0);
    }

    public BitfinexStatus status(){
        return (bitfinexFC.status().equals("[1]")?BitfinexStatus.READY:BitfinexStatus.MANTENAINCE);
    }

    @Cacheable("pairExchanges")
    public SendMessage getPairExchanges(TelegramCommand telegramCommand){
        logger.info("Llama al servicio bitfinex de symbols");
        SendMessage sendMessage;

        if (this.status()!=BitfinexStatus.READY){
            sendMessage = telegramCommand.sendMessage(String.format("Bitfinex api services down %s",
                emojiService.getEmojiUnicode(EmojiCtes.BROKEN_HEART)), false);
        }else {
            List<PairExchange> list = bitfinexToPojo.convertToListPairExchange(bitfinexFC.pairExchanges());
            StringBuilder out = new StringBuilder();
            for(var pair:list){
                out.append(String.format("%s:%s%n", pair.getBase(), pair.getRef()));
            }

            sendMessage = telegramCommand.sendMessage(out.toString(), false);
        }
        return sendMessage;
    }

    public SendMessage showAlarms(TelegramCommand telegramCommand) {

        Optional<List<DbAlarm>> alarms = alarmRepo.findByUserId(telegramCommand.getUserId());
        SendMessage sendMessage;

        if (alarms.isEmpty()){
            sendMessage = telegramCommand.sendMessage(String.format("<b>NO</b> alarms %s",
                    emojiService.getEmojiUnicode(EmojiCtes.MORNING_ALARM_CLOCK)
            ), true);
        }else{
            StringBuilder out = new StringBuilder();
            for(var alarm:alarms.get()){
                out.append(String.format("id: %d. pair: <b>%s:%s</b> %s value: <b>%f</b>%n",
                        alarm.getId(),
                        alarm.getBase(),
                        alarm.getReference(),
                        (alarm.getMaxvalue()==null?"min":"max"),
                        (alarm.getMaxvalue()==null?alarm.getMinvalue():alarm.getMaxvalue())
                        ));
            }
            sendMessage = telegramCommand.sendMessage(out.toString(), true);
        }

        return sendMessage;
    }
}
