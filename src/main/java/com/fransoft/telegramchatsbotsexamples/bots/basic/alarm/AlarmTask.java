package com.fransoft.telegramchatsbotsexamples.bots.basic.alarm;

import com.fransoft.telegramchatsbotsexamples.bots.basic.BitfinexBot;
import com.fransoft.telegramchatsbotsexamples.bots.basic.BitfinexBotProcessor;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex.PairExchange;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex.Ticker;
import com.fransoft.telegramchatsbotsexamples.persistence.entity.DbAlarm;
import com.fransoft.telegramchatsbotsexamples.persistence.repository.AlarmRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.HashMap;

@Component
public class AlarmTask {
    private final BitfinexBotProcessor bitfinexBotProcessor;
    private final BitfinexBot bitfinexBot;
    private final AlarmRepo alarmRepo;
    private static final Logger logger = LoggerFactory.getLogger(AlarmTask.class);
    private final HashMap<PairExchange, Ticker> tickers;


    public AlarmTask(BitfinexBotProcessor bitfinexBotProcessor, BitfinexBot bitfinexBot, AlarmRepo alarmRepo) {
        this.bitfinexBotProcessor = bitfinexBotProcessor;
        this.bitfinexBot = bitfinexBot;
        this.alarmRepo = alarmRepo;
        this.tickers = new HashMap<>();
    }

    @Scheduled(cron = "${cron.expression.check.alarms:-}")
    public void process(){
        logger.info("Arranca tarea de chequeo de alarmas");

        Iterable<DbAlarm> allArms = alarmRepo.findAll();
        for(var alarm: allArms){
            PairExchange pair = new PairExchange(alarm.getBase(), alarm.getReference());
            Ticker ticker;
            if (!tickers.containsKey(pair)){
                ticker = bitfinexBotProcessor.getTicker(pair);
                if (ticker == null){
                    continue;
                }else{
                    tickers.put(pair, ticker);
                }
            }else{
                ticker = tickers.get(pair);
            }
            if (alarm.getMaxvalue()!=null){
                if (ticker.getLast().compareTo(alarm.getMaxvalue())>0){
                    sendAlarm(alarm, String.format("%s:%s has raised max value %f > %f",
                            pair.getBase(), pair.getRef(), ticker.getLast(), alarm.getMaxvalue()));
                }
            }else if (alarm.getMinvalue()!=null && ticker.getLast().compareTo(alarm.getMinvalue())<0){
                sendAlarm(alarm, String.format("%s:%s has lowed min value %f < %f",
                        pair.getBase(), pair.getRef(), ticker.getLast(), alarm.getMinvalue()));
            }
        }
        tickers.clear();
        logger.info("Fin tarea de chequeo de alarmas");
    }

    private void sendAlarm(DbAlarm alarm, String msg) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(alarm.getChatId());

        sendMessage.setText(msg);
        bitfinexBot.sendMessage(sendMessage);

        alarmRepo.deleteById(alarm.getId());
    }

}
