package com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "bitfinex", url= "${bitfinex.api.basepath}")
public interface BitfinexFC {

    @RequestMapping(method = RequestMethod.GET, value="${bitfinex.api.ticker}")
    String ticker(@RequestParam(value="symbols") String symbols);

    @RequestMapping(method = RequestMethod.GET, value="${bitfinex.api.status}")
    String status();

    @RequestMapping(method = RequestMethod.GET, value="${bitfinex.api.pairexchanges}")
    String pairExchanges();

}
