package com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.converter;

import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex.PairExchange;
import com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex.Ticker;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class BitfinexToPojo {

    public List<Ticker> convertToListTickers(String in){
        return this.listTickerReadJson(in);
    }

    public List<PairExchange> convertToListPairExchange(String response){
        List<PairExchange> out = new ArrayList<>();
        String strBuff = response.substring(2, response.length()-2).replace("\"", "");
        String[] elements = strBuff.split(",");
        for(String element:elements){
            out.add(getBaseRef(element));
        }
        return out;
    }

    private PairExchange getBaseRef(String data){
        String base ;
        String ref;
        String[] pieces = data.startsWith("t")?data.substring(1).split(":"):data.split(":");
        if (pieces.length == 1){
            base = pieces[0].substring(0,3);
            ref = pieces[0].substring(3,6);
        }else{
            base = pieces[0];
            ref = pieces[1];
        }
        return new PairExchange(base, ref);
    }

    private List<Ticker> listTickerReadJson(String response){
        List<Ticker> out = new ArrayList<>();
        String strBuff = response.substring(1, response.length()-2);
        String[] pieces = strBuff.split("],");
        for(String piece:pieces){
            piece = piece.substring(1);
            String[] element = piece.split(",");
            PairExchange pair = getBaseRef(element[0].substring(1));
            out.add(Ticker.builder()
                    .base(pair.getBase())
                    .reference(pair.getRef())
                    .bid(new BigDecimal(element[1]))
                    .ask(new BigDecimal(element[3]))
                    .last(new BigDecimal(element[7]))
                    .high(new BigDecimal(element[9]))
                    .low(new BigDecimal(element[10]))
                    .volume(new BigDecimal(element[8]))
                    .build());
        }
        return out;
    }

}
