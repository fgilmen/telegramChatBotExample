package com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
@Data
@AllArgsConstructor
@EqualsAndHashCode
public class PairExchange {
    private String base;
    private String ref;
}
