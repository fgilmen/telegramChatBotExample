package com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
@Data
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class Ticker {
    private String base;
    private String reference;
    private BigDecimal bid;
    private BigDecimal ask;
    private BigDecimal last;
    private BigDecimal volume;
    private BigDecimal high;
    private BigDecimal low;
}
