package com.fransoft.telegramchatsbotsexamples.bots.basic.feignclients.pojos.bitfinex;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.sql.Timestamp;
@Data
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class Variation {
    private Timestamp upTime;
    private Timestamp downTime;
    private BigDecimal diff;
    private Float percentDiff;
    private BigDecimal upTimeValue;
    private BigDecimal downTimeValue;
}
