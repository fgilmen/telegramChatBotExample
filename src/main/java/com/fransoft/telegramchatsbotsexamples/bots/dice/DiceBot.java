package com.fransoft.telegramchatsbotsexamples.bots.dice;

import com.fransoft.telegramchatbot.AbstractTelegramLongPollingBot;
import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import com.fransoft.telegramchatsbotsexamples.bots.dice.exception.DiceTypeNotValidException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class DiceBot extends AbstractTelegramLongPollingBot {
    private static final String COMMAND_THROW_DICE = "throwdice";
    private final DiceBotProcessor diceBotProcessor;

    public DiceBot(
            @Value("${dicebot.name}") String botUsername,
            @Value("${dicebot.apikey}") String apiKey,
            @Value("${dicebot.function}") String botFunction,
            DiceBotProcessor diceBotProcessor) {
    	super(botUsername, apiKey, botFunction);
        this.diceBotProcessor = diceBotProcessor;
        initCommands();

    }

    private void initCommands() {
        CommandData commandDataThrowADice = CommandData.builder()
                .verb("/" + COMMAND_THROW_DICE)
                .options(getThrowADiceOptions())
                .presentation("throw a dice")
                .build();

        addCommand(COMMAND_THROW_DICE, commandDataThrowADice);
    }

    private Options getThrowADiceOptions() {
        Options options = new Options();
        Option option = new Option("t", "type", true,
                "dice type. 0 -> dice (1-6), 1 -> dart (1-5), 2 -> basket (0-1), 3 -> soccer (0-1) " +
                        "4 -> jackpot");
        option.setRequired(true);
        options.addOption(option);

        return options;
    }

    @Override
    protected void onReceived(final Update update, final TelegramCommand tc) {
        switch(tc.getVerb()){
            case COMMAND_THROW_DICE:
                processThrowDice(update);
                break;
            case COMMAND_HELP:
                printHelp(tc);
                break;
            default:
                noImplement(tc);
        }
    }

    private void processThrowDice(Update update) {

        CommandData commandData = getCommand(COMMAND_THROW_DICE);
        try {
            this.sendDice((this.diceBotProcessor.throwDice(TelegramCommand.build(update, commandData.getOptions()))));
        } catch (ParseException | DiceTypeNotValidException | NumberFormatException e) {
            printMessage(TelegramCommand.build(update), CommandLineUtils
                .getHelpCommandLine(commandData.getVerb(), commandData.getOptions(), commandData.getPresentation()));
        }
    }
}
