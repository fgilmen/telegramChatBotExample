package com.fransoft.telegramchatsbotsexamples.bots.dice;

import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatsbotsexamples.bots.dice.exception.DiceTypeNotValidException;
import java.util.List;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendDice;

@Component
public class DiceBotProcessor {
    private static final List<String> VALID_EMOJIS = List
        .of("\ud83c\udfb2", "\ud83c\udfaf", "\ud83c\udfc0", "⚽", "\ud83c\udfb0");


    public SendDice throwDice(TelegramCommand tc) throws DiceTypeNotValidException {
        int type = Integer.parseInt(tc.getParams().getOptionValue("t"));

        if (type > 4 || type < 0 ){
            throw new DiceTypeNotValidException("type number not valid");
        }
        return tc.sendDice(VALID_EMOJIS.get(type));
    }
}
