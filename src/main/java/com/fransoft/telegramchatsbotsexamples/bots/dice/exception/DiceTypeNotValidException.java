package com.fransoft.telegramchatsbotsexamples.bots.dice.exception;

public class DiceTypeNotValidException extends Exception{
    public DiceTypeNotValidException(String message) {
        super(message);
    }
}
