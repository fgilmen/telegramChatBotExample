package com.fransoft.telegramchatsbotsexamples.bots.doc;

import com.fransoft.telegramchatbot.AbstractTelegramLongPollingBot;
import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import com.fransoft.telegramchatsbotsexamples.bots.doc.exception.NotFoundDocumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class DocBot extends AbstractTelegramLongPollingBot {

    public static final String COMMAND_DOCS = "docs";
    public static final String COMMAND_DOCDOWNLOAD = "docdownload";
    private DocBotProcessor docBotProcessor;

    public DocBot(
            @Value("${docbot.name}") String botUsername,
            @Value("${docbot.apikey}") String apiKey,
            @Value("${docbot.function}") String botFunction,
            DocBotProcessor docBotProcessor) {
    	super(botUsername, apiKey, botFunction);
        this.docBotProcessor = docBotProcessor;

        initCommands();
    }

    @Override
    protected void onReceived(final Update update, final TelegramCommand tc) {
        switch(tc.getVerb()){
            case COMMAND_DOCS:
                processDocs(update);
                break;
            case COMMAND_DOCDOWNLOAD:
                processDocDownloads(update);
                break;
            case COMMAND_HELP:
                printHelp(tc);
                break;
            default:
                noImplement(tc);
        }
    }
    
    private void initCommands() {
        CommandData commandDataDocs = CommandData.builder()
                .verb("/"+COMMAND_DOCS)
                .options(getDocsOptions())
                .presentation("get doc list")
                .build();
        CommandData commandDataDownloadDocs = CommandData.builder()
                .verb("/"+COMMAND_DOCDOWNLOAD)
                .options(getDocDownloadOptions())
                .presentation("download a doc")
                .build();

        addCommand(COMMAND_DOCS, commandDataDocs);
        addCommand(COMMAND_DOCDOWNLOAD, commandDataDownloadDocs);
    }

    private Options getDocDownloadOptions() {
        Options options = new Options();
        Option option = new Option("n", "number", true, "number to download");
        option.setRequired(true);
        options.addOption(option);
        return options;
    }

    private Options getDocsOptions() {
        Options options = new Options();
        Option option = new Option("e", "extension", true, "extensions to get");
        option.setRequired(false);
        options.addOption(option);

        Option option2 = new Option("r", "recursive", false, "recursive search");
        option2.setRequired(false);
        options.addOption(option2);

        Option option3 = new Option("n", "name", true, "filename contains");
        option3.setRequired(false);
        options.addOption(option3);
        return options;
    }

    private void processDocDownloads(Update update) {
        CommandData commandData = getCommand(COMMAND_DOCDOWNLOAD);
        Options options = commandData.getOptions();
        try {
            SendDocument doc = this.docBotProcessor.getDoc(TelegramCommand.build(update, options));
            this.sendDocument(doc) ;
        } catch (ParseException e) {
            printMessage(TelegramCommand.build(update), CommandLineUtils
                .getHelpCommandLine(commandData.getVerb(), options, commandData.getPresentation()));
        } catch (NotFoundDocumentException e) {
            printMessage(TelegramCommand.build(update), e.getMessage());
        }
    }

    private void processDocs(Update update) {
        CommandData commandData = getCommand(COMMAND_DOCS);
        Options options = commandData.getOptions();
        try {
            this.sendMessage(this.docBotProcessor.getDocsName(TelegramCommand.build(update, options)));
        } catch (ParseException e) {
            printMessage(TelegramCommand.build(update), CommandLineUtils.getHelpCommandLine(commandData.getVerb(), options, commandData.getPresentation()));
        }
    }
}
