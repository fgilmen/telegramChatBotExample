package com.fransoft.telegramchatsbotsexamples.bots.doc.exception;

public class NotFoundDocumentException extends Exception{
    public NotFoundDocumentException(String message) {
        super(message);
    }
}
