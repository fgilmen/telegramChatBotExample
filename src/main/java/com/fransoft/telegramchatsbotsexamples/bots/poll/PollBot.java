package com.fransoft.telegramchatsbotsexamples.bots.poll;

import com.fransoft.telegramchatbot.AbstractTelegramLongPollingBot;
import com.fransoft.telegramchatbot.bo.CommandData;
import com.fransoft.telegramchatbot.bo.TelegramCommand;
import com.fransoft.telegramchatbot.utils.CommandLineUtils;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class PollBot extends AbstractTelegramLongPollingBot {

    public static final String COMMAND_POLLSTART = "pollstart";
    public static final String COMMAND_POLLSTOP = "pollstop";
    private final PollBotProcessor pollBotProcessor;
    private static final Logger logger = LoggerFactory.getLogger(PollBot.class);
    private Message messagePoll;

    public PollBot(@Value("${pollbot.name}") String botUsername,
                   @Value("${pollbot.apikey}") String apiKey,
                   @Value("${pollbot.function}") String botFunction,
                   PollBotProcessor pollBotProcessor) {
    	super(botUsername, apiKey, botFunction);
        this.pollBotProcessor = pollBotProcessor;

        initCommands();
    }

    private void initCommands() {
        CommandData commandDataPollStart = CommandData.builder()
                .verb("/"+COMMAND_POLLSTART)
                .options(this.getPollStartOptions())
                .presentation("start a poll")
                .build();
        CommandData commandDataPollStop = CommandData.builder()
                .verb("/"+COMMAND_POLLSTOP)
                .options(null)
                .presentation("stop a poll")
                .build();

        addCommand(COMMAND_POLLSTART, commandDataPollStart);
        addCommand(COMMAND_POLLSTOP, commandDataPollStop);
    }

    private Options getPollStartOptions() {
        Options options = new Options();

        Option oQuestion = new Option("q", "questions", true, "poll question");
        oQuestion.setRequired(true);
        options.addOption(oQuestion);

        Option oSeconds = new Option("s", "seconds", true, "poll duration in seconds");
        oSeconds.setRequired(false);
        options.addOption(oSeconds);

        Option oAsk = new Option("o", "options", true, "poll options");
        oAsk.setRequired(true);
        options.addOption(oAsk);
        return options;
    }

    @Override
    protected void onReceived(final Update update, final TelegramCommand tc) {
        if (update.getPollAnswer() != null){
            logger.info(String.format("%s votó %s", update.getPollAnswer().getUser().getUserName(),
                    this.pollBotProcessor.getPollOptions().get(update.getPollAnswer().getOptionIds().get(0))));
        }else{
            if (update.getPoll()!=null){
                if (update.getPoll().getIsClosed().equals(Boolean.TRUE)){
                    logger.info("Votación cerrada");
                }
            }else{
                switch(tc.getVerb()){
                    case COMMAND_POLLSTART:
                        CommandData commandData = this.getCommand(COMMAND_POLLSTART);
                        try {
                            messagePoll = this.sendPoll(this.pollBotProcessor.startPoll(TelegramCommand.build(update, commandData.getOptions())));
                        } catch (Exception e) {
                            try {
                                printMessage(TelegramCommand.build(update), CommandLineUtils
                                    .getHelpCommandLine(commandData.getVerb(), commandData.getOptions(), commandData.getPresentation()));
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                        break;
                    case COMMAND_POLLSTOP:
                        stopPoll(update);
                        break;
                    case COMMAND_HELP:
                        printHelp(tc);
                        break;
                    default:
                        noImplement(tc);
                }
            }
        }
    }

    private void stopPoll(Update update) {
        try {
            execute(this.pollBotProcessor.stopPoll(update, messagePoll));
        } catch (TelegramApiException e) {
            sendMessage(this.pollBotProcessor.stopPollError(update));
        }
    }

}
