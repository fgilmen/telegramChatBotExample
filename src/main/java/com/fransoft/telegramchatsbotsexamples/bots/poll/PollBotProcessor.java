package com.fransoft.telegramchatsbotsexamples.bots.poll;

import com.fransoft.telegramchatbot.bo.TelegramCommand;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.polls.StopPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class PollBotProcessor {
    private List<String> pollOptions;

    public List<String> getPollOptions() {
        return pollOptions;
    }

    public SendPoll startPoll(TelegramCommand tc) {

        SendPoll sendPoll = new SendPoll();
        sendPoll.setChatId(tc.getChatId());

        sendPoll.setQuestion(tc.getParams().getOptionValue("q"));
        sendPoll.setAllowMultipleAnswers(false);
        sendPoll.setIsAnonymous(false);
        if (tc.getParams().hasOption("s")){
            sendPoll.setOpenPeriod(Integer.valueOf(tc.getParams().getOptionValue("s")));
        }
        pollOptions = Stream.of(tc.getParams().getOptionValue("o").split(",", -1))
                .collect(Collectors.toList());
        sendPoll.setOptions(pollOptions);
        return sendPoll;
    }

    public StopPoll stopPoll(Update update, Message messagePoll) {
        StopPoll stopPoll = new StopPoll();
        stopPoll.setChatId(String.valueOf(update.getMessage().getChatId()));
        stopPoll.setMessageId(messagePoll.getMessageId());

        return stopPoll;
    }

    public SendMessage stopPollError(Update update){
        return TelegramCommand.build(update).sendMessage("ended poll", false);
    }

}
