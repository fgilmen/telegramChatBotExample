package com.fransoft.telegramchatsbotsexamples.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity(name = "Alarm")
@Table(name = "ALARMS")
@Data
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class  DbAlarm {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @Column
    private String base;
    @Column
    private String reference;
    @Column
    private BigDecimal maxvalue;
    @Column
    private BigDecimal minvalue;
    @Column(name="alarm_date")
    private Timestamp alarmDate;
    @Column(name="user_id")
    private Long userId;
    @Column
    private String chatId;
}
