package com.fransoft.telegramchatsbotsexamples.persistence.repository;


import com.fransoft.telegramchatsbotsexamples.persistence.entity.DbAlarm;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AlarmRepo extends CrudRepository<DbAlarm, Long> {
    Optional<List<DbAlarm>> findByUserId(Long userId);
}
